<?php

/**
 * Observes specific controller actions to remove NewRelic's real user monitoring (RUM) JavaScript.
 * This JavaScript can prevent Magento from operating correctly in some cases.  This is the case when loading new email
 * templates from the admin.  The NR agent embeds a JS snippit in the JSON response which causes Magento to fail to parse it.
 * See NR ticket #88658
 *
 * EYEMAGINE - The leading Magento Solution Partner
 *
 * NewRelic FIX
 *
 * @author    EYEMAGINE <magento@eyemaginetech.com>
 * @category  Eyemagine
 * @package   Eyemagine_NewRelicFix
 * @copyright Copyright (c) 2013 EYEMAGINE Technology, LLC (http://www.eyemaginetech.com)
 * @license   http://www.eyemaginetech.com/license.txt
 */

class Eyemagine_NewRelicFix_Model_Observer
{
    public function removeRumCode($observer) {
        if (extension_loaded('newrelic')) {
            newrelic_disable_autorum();
        }
    }
}